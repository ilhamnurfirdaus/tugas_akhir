<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::resource('profil', 'ProfilController');

Route::prefix("profil")->middleware('auth')->group(function() {
    Route::get('/', 'ProfilController@index');
    Route::get('/create', 'ProfilController@create');
    Route::post('/store', 'ProfilController@store');
    Route::post('/update/{id}', 'ProfilController@update');
    Route::post('/account/{id}', 'ProfilController@account');
    Route::post('/password/{id}', 'ProfilController@password');
});

Route::prefix("postingan")->middleware('auth')->group(function() {;
    Route::get('/', 'PostinganController@index');
    Route::post('/store', 'PostinganController@store');
    Route::post('/update/{id}', 'PostinganController@update');
    Route::get('/delete/{id}', 'PostinganController@destroy');
});

Route::prefix("search")->middleware('auth')->group(function() {;
    Route::get('/', 'SearchController@index');
    Route::get('/{id}', 'SearchController@show')->name('profil.show');
    Route::post('/update/{id}', 'SearchController@update');
    Route::get('/follow/{id}', 'SearchController@follow');
    Route::post('/unfollow/{id}', 'SearchController@unfollow');
});
