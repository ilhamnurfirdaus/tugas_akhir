<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikekomentarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likekomentars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('profil_id');
            $table->unsignedBigInteger('komentar_id');
            $table->integer('point')->length(11)->unsigned();
            $table->timestamps();

            $table->foreign('profil_id')->references('id')->on('profils')->onDelete('cascade');
            $table->foreign('komentar_id')->references('id')->on('komentars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likekomentars');
    }
}
