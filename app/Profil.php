<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = "profils";
    protected $fillable = ["user_id", "alamat", "tanggal_lahir", "nomor_hp", "foto_profil"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function followings()
    {
        return $this->hasMany('App\Following');
    }

    public function followers()
    {
        return $this->hasMany('App\Follower');
    }

    public function postingans()
    {
        return $this->hasMany('App\Postingan');
    }

    public function komentars()
    {
        return $this->hasMany('App\Komentar');
    }
    
    public function like_postingans()
    {
        return $this->hasMany('App\LikePostingan');
    }

    public function like_komentars()
    {
        return $this->hasMany('App\LikeKomentar');
    }
}
