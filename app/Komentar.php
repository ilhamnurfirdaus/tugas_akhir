<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = "komentars";
    protected $fillable = ["profil_id", "postingan_id", "isi"];

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }

    public function like_komentars()
    {
        return $this->hasMany('App\LikeKomentar');
    }
}
