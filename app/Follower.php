<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $table = "followers";
    protected $fillable = ["profil_id", "point", "follower_id"];

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }
}
