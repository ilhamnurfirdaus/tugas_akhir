<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profil;
use App\Rules\MatchOldPassword;
use App\User;
use App\Postingan;
use App\Following;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PostinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $profils = Profil::select('*');
        
        $profils = $profils->where('user_id', '=', $user->id)->first();

        $followings = Following::select('*');

        $followings = $followings->where('profil_id', '=', $profils->id)->first();

        $postingans = Postingan::select('*');

        $postingans = $postingans->where('profil_id', '=', $followings->following_id)->get();

        // return dd($postingans);

        return view('postingan.index', compact('user', 'profils', 'followings', 'postingans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $profil = Profil::select('*');

        $profil = $profil->where('user_id', '=', $user->id)->first();

        $postingan = $request->all();

        if(!isset($postingan['gambar_url'])){
            $postingan = $request->validate([
                'isi' => 'required',
            ]);
    
            $isi = $postingan['isi'];
    
            $data = new Postingan;
            $data->profil_id = $profil->id;
            $data->gambar_url = null;
    
            $data->isi = $isi;
            
            $data->save();
        } else {
            $postingan = $request->validate([
                'gambar_url' => 'required',
                'isi' => 'required',
            ]);
    
            $gambar_url = $postingan['gambar_url'];
            $isi = $postingan['isi'];
    
            $img_name = $gambar_url->getClientOriginalName();
    
            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
            $data = new Postingan;
            $data->profil_id = $profil->id;
    
            $new_image_name = $img_name_only.'('.$user->id.').'.$img_type;
            $data->gambar_url = $new_image_name;
    
            $data->isi = $isi;
    
            $path = 'public/user/assets/';
            $path = $postingan['gambar_url']->storeAs($path, $new_image_name);
            
            $data->save();
        }        

        return redirect('/profil')->with('success', 'Profil berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $profil = Profil::select('*');

        $profil = $profil->where('user_id', '=', $user->id)->first();

        $postingan = $request->all();

        if(!isset($postingan['gambar_url'])){
            $postingan = $request->validate([
                'isi' => 'required',
            ]);
    
            $isi = $postingan['isi'];
    
            $data = Postingan::find($id);
            $data->profil_id = $profil->id;
    
            $data->isi = $isi;
            
            $data->save();
        } else {
            $postingan = $request->validate([
                'gambar_url' => 'required',
                'isi' => 'required',
            ]);
    
            $gambar_url = $postingan['gambar_url'];
            $isi = $postingan['isi'];
    
            $img_name = $gambar_url->getClientOriginalName();
    
            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
            $data = Postingan::find($id);
            $data->profil_id = $profil->id;
    
            $new_image_name = $img_name_only.'('.$user->id.').'.$img_type;
            $data->gambar_url = $new_image_name;
    
            $data->isi = $isi;
    
            $path = 'public/user/assets/';
            $path = $postingan['gambar_url']->storeAs($path, $new_image_name);
            
            $data->save();
        }        

        return redirect('/profil')->with('success', 'Profil berhasil dibuat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Postingan::find($id);

        $data->delete();

        return back()->with('message', 'Data Deleted');
    }
}
