<?php

namespace App\Http\Controllers;

use App\Follower;
use App\Following;
use App\User;
use App\Profil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $profils = Profil::select('*');
        
        $search_filter = $request->search_filter;

        if($search_filter != null) {
            $profils = $profils->where('users.name', 'like', '%'.$search_filter.'%');
            $profils = $profils->orWhere('users.email', 'like', '%'.$search_filter.'%');
        }

        $profil = $profils->where('users.id', '!=', $user->id);

        $profils = $profils->leftJoin('users', 'profils.user_id', '=', 'users.id')
                        // ->whereNull('assets.deleted_at')
                        // ->whereNull('categories.deleted_at')
                        ->select('profils.*', 'users.*');

        $profils = $profils->orderBy("users.name", "ASC")->paginate(5);

        return view('search.index', compact('profils', 'search_filter'));
        // return dd($profils);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auth = Auth::user();

        $profil = Profil::all()->where('user_id', $id)->first();
        $user = User::all()->where('id', $id)->first();
        $postingans = $profil->postingans()->get();
        
        $following = Following::where('profil_id', $profil->id)->get()->sum('point');
        
        $follower = Follower::where('profil_id', $profil->id)->get()->sum('point');

        $follow = Following::all()->where('profil_id', $auth->id)->where('following_id', $profil->id)->first();

        // return dd($follow);
        return view('search.show', compact('profil', 'user', 'postingans', 'following', 'follower', 'follow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $auth = Auth::user();
        $profil = Profil::all()->where('user_id', $auth->id)->first();

        $data1 = Follower::find($id);
        $data1->profil_id = $id;
        $data1->point = 1;
        $data1->follower_id = $profil->id;

        $data2 = Following::find($id);
        $data2->profil_id = $profil->id;
        $data2->point = 1;
        $data2->following_id = $id;

        $data1->save();
        $data2->save();

        return back()->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function follow($id)
    {
        $auth = Auth::user();
        $profil = Profil::all()->where('user_id', $auth->id)->first();

        $data1 = new Follower;
        $data1->profil_id = $id;
        $data1->point = 1;
        $data1->follower_id = $profil->id;

        $data2 = new Following;
        $data2->profil_id = $profil->id;
        $data2->point = 1;
        $data2->following_id = $id;

        $data1->save();
        $data2->save();

        return back()->with('success', 'Data berhasil dibuat');
    }

    public function unfollow(Request $request, $id)
    {
        // $auth = Auth::user();
        // $profil = Profil::all()->where('user_id', $auth->id)->first();

        $data1 = Follower::find($id);
        return dd($data1);
        // $data1->profil_id = $id;
        $data1->point = 0;
        // $data1->follower_id = $profil->id;

        $data2 = Following::find($id);
        // $data2->profil_id = $profil->id;
        $data2->point = 0;
        // $data2->following_id = $id;

        $data1->save();
        $data2->save();

        return back()->with('success', 'Data berhasil diupdate');
    }
}
