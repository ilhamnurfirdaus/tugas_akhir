<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profil;
use App\Rules\MatchOldPassword;
use App\User;
use App\Postingan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Following;
use App\Follower;

class ProfilController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil = Profil::select('*');
        $user = Auth::user();

        // $postingans = Postingan::select('*');
        
        // $postingan = $postingan->profil();
        
        $profil = $profil->where('user_id', '=', $user->id)->first();

        $postingans = $profil->postingans()->get();

        $following = Following::where('profil_id', $profil->id)->get()->sum('point');

        $follower = Follower::where('profil_id', $profil->id)->get()->sum('point');

        // $postingans = $postingans->where('profil_id', $profil->id)->get();

        // $profil = $profil->where('profils.user_id', '=', $user->id);

        // $profil = $profil->leftJoin('users', 'profils.user_id', '=', 'users.id')
        //                 ->select('profils.*', 'users.name', 'users.email', 'users.password')->get();
        
        // $profil = $profil->user();
        // $profil = $profil->follower(); // array
        return view('profil.index', compact('profil', 'user', 'postingans'));
        // return dd($postingans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $user = Auth::user();
        $profil = $request->validate([
            'alamat' => 'required',
            'tanggal_lahir' => 'required',
            'nomor_hp' => 'required',
            'foto_profil' => 'required',
        ]);

        $alamat = $profil['alamat'];
        $tanggal_lahir = $profil['tanggal_lahir'];
        $nomor_hp = $profil['nomor_hp'];
        $foto_profil = $profil['foto_profil'];

        $img_name = $foto_profil->getClientOriginalName();

        $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
        $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

        $data = new Profil;
        $data->user_id = $user->id;
        $data->alamat = $alamat;
        $data->tanggal_lahir = $tanggal_lahir;
        $data->nomor_hp = $nomor_hp;
        $new_image_name = $img_name_only.'('.$user->id.').'.$img_type;
        $data->foto_profil = $new_image_name;

        // $data->foto_profil = 'foto_profil.png';
        // $data->foto_profil = NULL;

        $path = 'public/user/assets/';
        $path = $profil['foto_profil']->storeAs($path, $new_image_name);
        
        $data->save();

        return redirect('/profil')->with('success', 'Profil berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profil = $request->all();


        if(!isset($profil['foto_profil'])){
            $profil = $request->validate([
                'alamat' => 'required',
                'tanggal_lahir' => 'required',
                'nomor_hp' => 'required',
            ]);

            $alamat = $profil['alamat'];
            $tanggal_lahir = $profil['tanggal_lahir'];
            $nomor_hp = $profil['nomor_hp'];

            $data = Profil::find($id);
            $data->alamat = $alamat;
            $data->tanggal_lahir = $tanggal_lahir;
            $data->nomor_hp = $nomor_hp;
            $data->save();
        } else {
            $profil = $request->validate([
                'alamat' => 'required',
                'tanggal_lahir' => 'required',
                'nomor_hp' => 'required',
                'foto_profil' => 'required',
            ]); 

            $alamat = $profil['alamat'];
            $tanggal_lahir = $profil['tanggal_lahir'];
            $nomor_hp = $profil['nomor_hp'];
            $foto_profil = $profil['foto_profil'];

            $img_name = $foto_profil->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $data = Profil::find($id);
            $data->alamat = $alamat;
            $data->tanggal_lahir = $tanggal_lahir;
            $data->nomor_hp = $nomor_hp;
            $oldFile = 'public/user/assets/'.$data->foto_profil;
            Storage::delete([$oldFile]);
            $new_image_name = $img_name_only.'('.$id.').'.$img_type;
            $data->foto_profil = $new_image_name;
        
            $data->save();

            $path = 'public/user/assets/';
            $path = $profil['foto_profil']->storeAs($path, $new_image_name);
        }

        return back()->with('message', 'Data successfully change');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function account(Request $request, $id)
    {
        // $user = $request->validate([
        //     'name' => 'required|string|max:255',
        //     'email' => 'required|string|email|max:255|unique:users',
        // ]);
        // return dd($request);
        $user = $request->all();

        $name = $user['name'];
        $email = $user['email'];

        $data = User::find($id);
        $data->name = $name;
        $data->email = $email;

        $data->save();

        return back()->with('message', 'Data successfully change');
    }

    public function password(Request $request, $id)
    {
        $user = $request->validate([
            'current_password' => 'required', new MatchOldPassword,
            'new_password' => 'required|string|min:8|confirmed',
            'new_confirm_password' => 'same:new_password',
        ]);
        // return dd($request);
        // $user = $request->all();

        $new_password = $user['new_password'];

        $data = User::find($id);
        $data->password = Hash::make($new_password);

        $data->save();

        return back()->with('message', 'Data successfully change');
    }
}
