<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePostingan extends Model
{
    protected $table = "likepostingans";
    protected $fillable = ["profil_id", "postingan_id", "point"];

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }
}
