<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    protected $table = "postingans";
    protected $fillable = ["profil_id", "gambar_url", "isi"];

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }

    public function komentars()
    {
        return $this->hasMany('App\Komentar');
    }
    
    public function like_postingans()
    {
        return $this->hasMany('App\LikePostingan');
    }
}
