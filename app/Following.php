<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Following extends Model
{
    protected $table = "followings";
    protected $fillable = ["profil_id", "point", "following_id"];

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }
}
