<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeKomentar extends Model
{
    protected $table = "likekomentars";
    protected $fillable = ["profil_id", "komentar_id", "point"];

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }

    public function komentar()
    {
        return $this->belongsTo('App\Komentar');
    }
}
