@extends('layouts.app')

@section('title', 'Search')

@section('content')
<div class="container">
    <div class="main-body">
        <form action="{{ action('SearchController@index') }}">
            <div class="input-group mb-3">        
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1" for="search">Search</span>
                </div>
                <input type="text" class="form-control" placeholder="Search" aria-span="Search" aria-describedby="basic-addon1" name="search_filter" id="search" value="{{ Request::get('search_filter') }}">
                <div class="input-group-append">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </div>
            </div>
        </form>
        @foreach ($profils as $profil)
        <div class="row my-5">
            <div class="col-1 d-flex justify-content-center">
                <img class="rounded-circle" alt="Cinque Terre" src="{{ url('storage/user/assets/'.$profil->foto_profil) }}" style="max-width: 100px; max-height: 100px">
            </div>
            <div class="col-2 pl-5">
                <div class="row">
                    <p>Nama</p>
                </div>
                <div class="row">
                    <p>Email</p>
                </div>
                <div class="row">
                    <p>Nomor Hp</p>
                </div>
            </div>
            <div class="col-3">
                <div class="row">
                    <p>: {{$profil->name}}</p>
                </div>
                <div class="row">
                    <p>: {{$profil->email}}</p>
                </div>
                <div class="row">
                    <p>: {{$profil->nomor_hp}}</p>
                </div>
            </div>
            <div class="col-6 d-flex justify-content-end">
                <div class="row">
                    <button type="button" class="btn btn-info my-2"
                        onclick="event.preventDefault();
                        window.location = '{{ route('profil.show', $profil->user_id) }}';">
                        Info
                    </button>   
                </div>
            </div>
        </div>
        @endforeach
        <div class="my-3">
            {{ $profils->appends(request()->except('page'))->links() }}
        </div>
    </div>
</div>
@endsection

@push('script')

@endpush
