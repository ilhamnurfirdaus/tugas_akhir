@extends('layouts.app')

@section('title', 'Profil')

@section('content')
<div class="container">
    <div class="main-body">
        {{-- @if(session()->has('message'))
            <div class="alert alert-success" role="alert" style="margin-bottom: 16px">
                {{ session()->get('message') }}
            </div>
        @endif
        @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" role="alert" style="margin-bottom: 16px">
                        <p class="text-danger">{{ $error }}</p>
                    </div>
        @endforeach --}}
        <div class="row bg-white text-dark">
            <div class="col d-flex justify-content-center my-3">
              @if (isset($profil->foto_profil))
                  <img class="rounded-circle" alt="Cinque Terre" src="{{ url('storage/user/assets/'.$profil->foto_profil) }}" style="width: 150px">        
              @else
                  <h1 class="logo-badge text-whitesmoke"><span class="fa fa-user-circle fa-3x"></span></h1>
              @endif
            </div>
          </div>
          <div class="row bg-white text-dark">
              <div class="col d-flex justify-content-center mb-3">
                 <h2>{{$user->name}}({{$user->email}})</h2>
              </div>
          </div>
          <div class="row bg-white text-dark mb-3">
            <div class="col-6 d-flex justify-content-end">
               
                @if (!isset($following))
                <h2>Following : 0 </h2>
                @else
                <h2>Following : {{$following}} </h2>
                @endif
               </h2>
            </div>
            <div class="col-6">    
                @if (!isset($follower))
                    <h2>Followers : 0</h2>
                @else
                    <h2>Followers : {{$follower}}</h2>
                @endif
            </div>
        </div>
        <div class="row bg-white text-dark">
              <div class="col d-flex justify-content-center mb-3">
                @if (!isset($follow->point))
                <form action="{{action('SearchController@follow', $profil->id)}}" method="GET" enctype="multipart/form-data">
                    @csrf
                    <button type="submit" class="btn btn-primary">Follow</button>  
                </form>
                @elseif ($follow->point = 0)
                <form action="{{action('SearchController@update', $follow->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input class="form-control" type="text" name="point" value="1" readonly hidden>
                    <button type="submit" class="btn btn-primary">Follow</button>
                </form>
                @else
                <form action="{{action('SearchController@unfollow', $follow->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input class="form-control" type="text" name="point" value="0" readonly hidden>
                    <button type="submit" class="btn btn-danger">Unfollow</button>
                </form>
                @endif
              </div>
        </div>
          <div class="row bg-white text-dark my-5">
            
            <div class="col-6">
                <div class="row">
                    <div class="col-3">
                        <h5>Alamat</h5>
                    </div>
                    <div class="col-9">
                        <h5>: {{$profil->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <h5>Tanggal Lahir</h5>
                    </div>
                    <div class="col-9">
                        <h5>: {{$profil->tanggal_lahir}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <h5>Nomor HP</h5>
                    </div>
                    <div class="col-9">
                        <h5>: {{$profil->nomor_hp}}</h5>
                    </div>
                </div>
            </div>
            <div class="col-6">
                @foreach ($postingans as $postingan)
                <div class="row">
                    <div class="col d-flex justify-content-center mb-2">
                        @if (isset($postingan->gambar_url))
                            <img class="img-fluid" alt="Cinque Terre" src="{{ url('storage/user/assets/'.$postingan->gambar_url) }}" style="max-width: 550px">         
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p>{{$postingan->isi}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>
@endsection

@push('script')
    
@endpush
