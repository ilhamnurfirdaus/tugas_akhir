@extends('layouts.app')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="main-body">
        @foreach ($postingans as $postingan)
            <div class="row">
                <div class="col mb-2">
                    @if (isset($postingan->gambar_url))
                        <img class="img-fluid" alt="Cinque Terre" src="{{ url('storage/user/assets/'.$postingan->gambar_url) }}" style="max-width: 550px">         
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p>{{$postingan->isi}}</p>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

@push('script')

@endpush
