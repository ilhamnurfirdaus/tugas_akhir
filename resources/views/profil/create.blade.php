@extends('layouts.app')
{{-- @extends('layouts.account')

@section('title', 'Profil')

@section('name', 'Profil') --}}

@section('content')
{{-- <form class="margin-t" method="POST" action="{{ action('ProfilController@store') }}" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
        <label for="alamat"  class="text-darkyellow d-flex justify-content-start" >Alamat</label>
        <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="tanggal_lahir"  class="text-darkyellow d-flex justify-content-start" >Tanggal Lahir</label>
        <input class="form-control" type="date" value="0000-00-00" id="tanggal_lahir" name="tanggal_lahir">
    </div>

    <div class="form-group">
        <label for="nomor_hp"  class="text-darkyellow d-flex justify-content-start" >Nomor HP</label>
        <input type="text" class="form-control" name="nomor_hp" id="nomor_hp" placeholder="Masukkan Nomor Hp">
        @error('nomor_hp')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="form-button button-l margin-b">Register</button>

</form> --}}

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register</div>

                <div class="card-body">
                    <form method="POST" action="{{ action('ProfilController@store') }}" enctype="multipart/form-data">
                        @csrf

                        {{-- <h1 class="logo-badge text-whitesmoke d-flex justify-content-center mb-3"><span class="fa fa-user-circle fa-3x"></span></h1> --}}

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Foto Profil</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control-file" name="foto_profil" id="foto_profil">

                                @error('foto_profil')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">

                                @error('alamat')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">Tanggal Lahir</label>
                            <div class="col-md-6">
                              <input class="form-control" type="date" value="0000-00-00" id="tanggal_lahir" name="tanggal_lahir">
                        </div>
                        
                        </div>

                        <div class="form-group row">
                            <label for="nomor_hp" class="col-md-4 col-form-label text-md-right">Nomor Hp</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nomor_hp" id="nomor_hp" placeholder="Masukkan Nomor Hp">

                                @error('nomor_hp')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
