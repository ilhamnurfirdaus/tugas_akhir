@extends('layouts.app')

@section('title', 'Profil')

@section('content')
<div class="container">
    <div class="main-body">
        @if(session()->has('message'))
            <div class="alert alert-success" role="alert" style="margin-bottom: 16px">
                {{ session()->get('message') }}
            </div>
        @endif
        @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" role="alert" style="margin-bottom: 16px">
                        <p class="text-danger">{{ $error }}</p>
                    </div>
        @endforeach
        <div class="row bg-white text-dark">
            <div class="col d-flex justify-content-center my-3">
              @if (isset($profil->foto_profil))
                  <img class="rounded-circle" alt="Cinque Terre" src="{{ url('storage/user/assets/'.$profil->foto_profil) }}" style="width: 150px">        
              @else
                  <h1 class="logo-badge text-whitesmoke"><span class="fa fa-user-circle fa-3x"></span></h1>
              @endif
            </div>
          </div>
          <div class="row bg-white text-dark">
              <div class="col d-flex justify-content-center mb-3">
                 <h2>{{$user->name}}({{$user->email}})</h2>
              </div>
          </div>
          <div class="row bg-white text-dark mb-3">
            <div class="col-6 d-flex justify-content-end">
                @if (!isset($following))
                <h2>Following : 0 </h2>
                @else
                <h2>Following : {{$following}} </h2>
                @endif
               </h2>
            </div>
            <div class="col-6">
                @if (!isset($follower))
                    <h2>Followers : 0</h2>
                @else
                    <h2>Followers : {{$follower}}</h2>
                @endif
             </div>
        </div>
        <div class="row bg-white text-dark mb-3">
            <div class="col d-flex justify-content-center">
                <button type="button" class="btn btn-primary mx-3" data-toggle="modal" data-target="#update-modal-{{ $user['id'] }}">Edit Profil</button>
                <button type="button" class="btn btn-primary mx-3" data-toggle="modal" data-target="#account-modal-{{ $user['id'] }}">Edit Account</button>
                <button type="button" class="btn btn-primary mx-3" data-toggle="modal" data-target="#password-modal-{{ $user['id'] }}">Edit Password</button>
            </div>
        </div>
        <div class="row bg-white text-dark my-5">
            <div class="col-6">
                <div class="row">
                    <div class="col-3">
                        <h5>Alamat</h5>
                    </div>
                    <div class="col-9">
                        <h5>: {{$profil->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <h5>Tanggal Lahir</h5>
                    </div>
                    <div class="col-9">
                        <h5>: {{$profil->tanggal_lahir}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <h5>Nomor HP</h5>
                    </div>
                    <div class="col-9">
                        <h5>: {{$profil->nomor_hp}}</h5>
                    </div>
                </div>
            </div>
            <div class="col-6">
                @foreach ($postingans as $postingan)
                <div class="row">
                    <div class="col d-flex justify-content-center mb-2">
                        @if (isset($postingan->gambar_url))
                            <img class="img-fluid" alt="Cinque Terre" src="{{ url('storage/user/assets/'.$postingan->gambar_url) }}" style="max-width: 550px">         
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p>{{$postingan->isi}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col mb-5">
                        <button type="button" class="btn btn-primary mx-3" data-toggle="modal" data-target="#update-postingan-modal-{{ $postingan['id'] }}">Edit Postingan</button>
                        <button type="button" data-toggle="modal" data-target="#delete-postingan-modal-{{ $postingan['id'] }}" class="btn btn-danger">Delete</button>
                    </div>
                </div>
                @endforeach
                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-primary mb-5" data-toggle="modal" data-target="#create-postingan-modal">Buat Postingan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Create Modal --}}
    <div class="modal fade" id="create-postingan-modal" tabindex="-1" role="dialog" aria-labelledby="create-postingan-modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="create-postingan-modalLabel">Create Posting</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="create-postingan" action="{{action('PostinganController@store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="gambar_url" class="col-md-4 col-form-label text-md-right">Gambar Postingan</label>

                        <div class="input-group col-md-8">
                            <input type="file" class="form-control-file" name="gambar_url" id="gambar_url">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="isi" class="col-md-4 col-form-label text-md-right">Caption</label>

                        <div class="input-group col-md-8">
                            <textarea class="form-control" id="isi" name="isi" rows="10"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onclick="btnCreatePostingan()">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade" id="update-modal-{{ $user['id'] }}" tabindex="-1" role="dialog" aria-labelledby="update-modal-{{ $user['id'] }}Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="update-modal-{{ $user['id'] }}Label">Update {{ $profil['name'] }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="update-{{ $user['id'] }}" action="{{action('ProfilController@update', $user['id'])}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="foto_profil" class="col-md-4 col-form-label text-md-right">Foto Profil</label>

                        <div class="input-group col-md-8">
                            <input type="file" class="form-control-file" name="foto_profil" id="foto_profil">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                        <div class="input-group col-md-8">
                            <input id="alamat" type="text" class="form-control" name="alamat" value="{{$profil['alamat']}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">Tanggal Lahir</label>

                        <div class="input-group col-md-8">
                            <input id="tanggal_lahir" type="date" class="form-control" name="tanggal_lahir" value="{{$profil['tanggal_lahir']}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nomor_hp" class="col-md-4 col-form-label text-md-right">Nomor HP</label>

                        <div class="input-group col-md-8">
                            <input id="nomor_hp" type="text" class="form-control" name="nomor_hp" value="{{$profil['nomor_hp']}}">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onclick="btnUpdate({{ $user['id'] }})">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade" id="account-modal-{{ $user['id'] }}" tabindex="-1" role="dialog" aria-labelledby="account-modal-{{ $user['id'] }}Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="account-modal-{{ $user['id'] }}Label">Update {{ $profil['name'] }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="account-{{ $user['id'] }}" action="{{action('ProfilController@account', $user['id'])}}" method="POST" enctype="multipart/form-data">
                
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                        <div class="input-group col-md-8">
                            <input id="name" type="text" class="form-control" name="name" value="{{$user['name']}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                        <div class="input-group col-md-8">
                            <input id="email" type="text" class="form-control" name="email" value="{{$user['email']}}" >
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onclick="btnAccount({{ $user['id'] }})">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade" id="password-modal-{{ $user['id'] }}" tabindex="-1" role="dialog" aria-labelledby="password-modal-{{ $user['id'] }}Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="password-modal-{{ $user['id'] }}Label">Update {{ $profil['name'] }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="password-{{ $user['id'] }}" action="{{action('ProfilController@password', $user['id'])}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="password" class="col-md-5 col-form-label text-md-left">Current Password</label>

                        <div class="input-group col-md-7" id="show_hide_password">
                            <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            <div class="input-group-append">
                                <div class="input-group-text" style="max-width: 44px">
                                    <a class="fa fa-eye" onclick="myFunction1()" aria-hidden="true"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-group row">
                        <label for="password" class="col-md-5 col-form-label text-md-left">New Password</label>

                        <div class="input-group col-md-7" id="show_hide_new_password">
                            <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            <div class="input-group-append">
                                <div class="input-group-text" style="max-width: 44px">
                                    <a class="fa fa-eye" onclick="myFunction2()" aria-hidden="true"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-5 col-form-label text-md-left">New Confirm Password</label>

                        <div class="input-group col-md-7" id="show_hide_new_confirm_password">
                            <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            <div class="input-group-append">
                                <div class="input-group-text" style="max-width: 44px">
                                    <a class="fa fa-eye" onclick="myFunction3()" aria-hidden="true"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onclick="btnPassword({{ $user['id'] }})">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    @foreach ($postingans as $postingan)
    <div class="modal fade" id="update-postingan-modal-{{ $postingan['id'] }}" tabindex="-1" role="dialog" aria-labelledby="update-postingan-modal-{{ $postingan['id'] }}Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="update-postingan-modal-{{ $postingan['id'] }}Label">Update {{ $postingan['id']  }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form id="update-postingan-{{ $postingan['id'] }}" action="{{action('PostinganController@update', $postingan['id'])}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="gambar_url" class="col-md-4 col-form-label text-md-right">Gambar Postingan</label>

                        <div class="input-group col-md-8">
                            <input type="file" class="form-control-file" name="gambar_url" id="gambar_url">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="isi" class="col-md-4 col-form-label text-md-right">Caption</label>

                        <div class="input-group col-md-8">
                            <textarea class="form-control" id="isi" name="isi" rows="10" >{{$postingan['isi']}}</textarea>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onclick="btnUpdatePostingan({{ $postingan['id'] }})">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    {{-- Delete Modal --}}
    <div class="modal fade" id="delete-postingan-modal-{{ $postingan['id']}}" tabindex="-1" role="dialog" aria-labelledby="delete-postingan-modal-{{ $postingan['id']}}Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="delete-postingan-modal-{{ $postingan['id']}}Label">Delete {{ $postingan['id'] }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus {{ $postingan['id'] }}?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="{{action('PostinganController@destroy', $postingan['id'])}}" method="GET" enctype="multipart/form-data">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
        </div>
    </div>
    @endforeach

</div>
@endsection

@push('script')
    <script>
        function btnCreatePostingan(){
            console.log("YEAH");
            document.getElementById('create-postingan').submit();
        }

        function btnUpdate(id){
            console.log("YEAH");
            document.getElementById('update-'+ id).submit();
        }

        function btnAccount(id){
            console.log("YEAH");
            document.getElementById('account-'+ id).submit();
        }

        function btnPassword(id){
            console.log("YEAH");
            document.getElementById('password-'+ id).submit();
        }

        function btnUpdatePostingan(id){
            console.log("YEAH");
            document.getElementById('update-postingan-'+ id).submit();
        }

        function myFunction1() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
                $('#show_hide_password a').addClass( "fa-eye-slash" );
                $('#show_hide_password a').removeClass( "fa-eye" );
            } else {
                x.type = "password";
                $('#show_hide_password a').addClass( "fa-eye" );
                $('#show_hide_password a').removeClass( "fa-eye-slash" );
            }
        }

        function myFunction2() {
            var x = document.getElementById("new_password");
            if (x.type === "password") {
                x.type = "text";
                $('#show_hide_new_password a').addClass( "fa-eye-slash" );
                $('#show_hide_new_password a').removeClass( "fa-eye" );
            } else {
                x.type = "password";
                $('#show_hide_new_password a').addClass( "fa-eye" );
                $('#show_hide_new_password a').removeClass( "fa-eye-slash" );
            }
        }

        function myFunction3() {
            var x = document.getElementById("new_confirm_password");
            if (x.type === "password") {
                x.type = "text";
                $('#show_hide_new_confirm_password a').addClass( "fa-eye-slash" );
                $('#show_hide_new_confirm_password a').removeClass( "fa-eye" );
            } else {
                x.type = "password";
                $('#show_hide_new_confirm_password a').addClass( "fa-eye" );
                $('#show_hide_new_confirm_password a').removeClass( "fa-eye-slash" );
            }
        }

    </script>
@endpush
